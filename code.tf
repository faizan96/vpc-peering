

provider "aws" {
  region = "us-east-1"
}



resource "aws_vpc" "vpc1" {
  cidr_block = "10.0.0.0/16"
  enable_dns_support = true
  enable_dns_hostnames = true
  tags = {
    Name = "VPC1"
  }
}

resource "aws_vpc" "vpc2" {
  cidr_block = "10.1.0.0/16"
  enable_dns_support = true
  enable_dns_hostnames = true
  tags = {
    Name = "VPC2"
  }
}

# Create VPC peering connection
resource "aws_vpc_peering_connection" "vpc_peering" {
  peer_vpc_id = aws_vpc.vpc2.id
  vpc_id      = aws_vpc.vpc1.id

  auto_accept = true

  tags = {
    Name = "VPC-Peering"
  }
}